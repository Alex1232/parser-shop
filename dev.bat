@echo off

pushd %~dp0

if not exist venv (
  ECHO "INIT..."
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit

CALL python -m pip install --upgrade pip==21.0.1 pip-tools==6.0.1 doit==0.33.1 || GOTO :exit

CALL python -m pip install --upgrade -r requirements\dev.txt || GOTO :exit

:exit
popd
if "%errorlevel%" NEQ "0" exit /b %errorlevel%

