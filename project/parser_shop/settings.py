import os
from pathlib import Path

from parser.apps.aliexpress import AliExpress
from parser.apps.citilink import Citilink
from parser.apps.kazan_express import Kazanexpress
from parser.apps.lamoda import Lamoda
from parser.apps.market_yandex import MarketYandex
from parser.apps.ozon import Ozon
from parser.apps.sber_mega_market import SberMegaMarket
from parser.apps.wildberries import Wildberries

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = 'django-insecure-qhsj7dcgj_f45xbbanrfxyi9ufi&9*4_*f)a=(%=1#ijzi_xi='
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'parser',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'parser_shop.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'parser_shop.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

STATIC_URL = 'static/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Список доступных для парсинга магазинов
SHOPS = {
    'Wildberries': Wildberries,
    # 'Ozon': Ozon,
    # 'MarketYandex': MarketYandex,
    'Kazanexpress': Kazanexpress,
    'Citilink': Citilink,
    'Lamoda': Lamoda,
    'AliExpress': AliExpress,
    'SberMegaMarket': SberMegaMarket,
}


# Основной url для управления медиафайлами
MEDIA_URL = '/media/'

# Путь хранения картинок
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/media/image')