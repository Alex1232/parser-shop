from django.contrib import admin
from django.urls import path
from parser.business_logic.views import ParserFormView, collect_parser_data
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ParserFormView.as_view()),
    path('collect/', collect_parser_data)
]

urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)