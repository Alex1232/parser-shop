
from parser.apps.wildberries.beautiful_soup import WildberriesBeautifulSoup
from parser.apps.wildberries.selenium import WildberriesSelenium


class Wildberries(WildberriesBeautifulSoup, WildberriesSelenium):
    img = 'https://mrkblog.ru/wp-content/uploads/2021/03/wildberries.jpg'
    url = 'https://www.wildberries.ru'

    def __str__(self):
        return 'Wildberries'
