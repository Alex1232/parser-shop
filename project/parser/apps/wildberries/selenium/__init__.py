from typing import NoReturn

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver

from parser.apps import InitialSelenium


class WildberriesSelenium(InitialSelenium):

    def get_url_for_parser(self) -> str:
        return super(WildberriesSelenium, self).get_url_for_parser()

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        return super(WildberriesSelenium, self).get_element_by_silenium(driver, type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        return super(WildberriesSelenium, self).get_html(driver)

    def create_options(self) -> webdriver.ChromeOptions():
        return super(WildberriesSelenium, self).create_options()

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        return super(WildberriesSelenium, self).create_web_driver(option)

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str):
        return super(WildberriesSelenium, self).record_html_in_driver_by_url(driver, url)

    def initialization_search_by_html(self, driver) -> NoReturn:
        # TODO Сделать красиво
        search_input = driver.find_element_by_id('searchInput')
        search_input.send_keys(f'{self.product}')
        search_input.send_keys(Keys.ENTER)
        super(WildberriesSelenium, self).initialization_search_by_html(driver)