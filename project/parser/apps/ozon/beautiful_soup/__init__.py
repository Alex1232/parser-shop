
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class OzonBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'x6i xi7'})

    def get_clean_link(self, product_card):
        try:
            return product_card.find('a', {'class': 'product-card__main j-card-link'}).attrs['href'],
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return product_card.find('img', {'class': 'j-thumbnail thumbnail'}).attrs['src'],
        except AttributeError:
            raise

    def get_clean_price(self, product_card):
        try:
            return product_card.find('span', {'class': 'ui-u ui-u1 ui-u6'}).text.strip()[:-1].strip()
        except AttributeError:
            return product_card.find('span', {'class': 'lower-price'}).text.strip()[:-1].strip()

    def get_clean_brand_name(self, product_card):
        try:
            return product_card.find('strong', {'class': 'brand-name'}).text
        except AttributeError:
            return ''

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('span', {'class': 'goods-name'}).text,
        except AttributeError:
            raise

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(OzonBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)
