from typing import NoReturn

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

from parser.apps import InitialSelenium


class OzonSelenium(InitialSelenium):

    def get_url_for_parser(self) -> str:
        return super(OzonSelenium, self).get_url_for_parser()

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        return super(OzonSelenium, self).get_element_by_silenium(driver, type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        return super(OzonSelenium, self).get_html(driver)

    def create_options(self) -> webdriver.ChromeOptions():
        return super(OzonSelenium, self).create_options()

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        return super(OzonSelenium, self).create_web_driver(option)

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str):
        return super(OzonSelenium, self).record_html_in_driver_by_url(driver, url)

    def initialization_mov_enter(self, search) -> NoReturn:
        return super(OzonSelenium, self).initialization_mov_enter(search)

    def initialization_search_by_html(self, driver):
        # TODO Сделать красиво
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="stickyHeader"]/div[3]/div/form/div[2]/input[1]'))).send_keys(f'{self.product}')
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="stickyHeader"]/div[3]/div/form/div[2]/input[1]'))).send_keys(Keys.ENTER)
        super(OzonSelenium, self).initialization_search_by_html(driver)