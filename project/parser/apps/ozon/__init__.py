from parser.apps.ozon.beautiful_soup import OzonBeautifulSoup
from parser.apps.ozon.selenium import OzonSelenium


class Ozon(OzonBeautifulSoup, OzonSelenium):
    img = 'https://static.tildacdn.com/tild3133-3130-4164-b264-643634383539/Ozon2.png'
    url = 'https://www.ozon.ru'

    def __str__(self):
        return 'Ozon'
