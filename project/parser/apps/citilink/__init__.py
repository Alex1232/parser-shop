from parser.apps.citilink.beautiful_soup import CitilinkBeautifulSoup
from parser.apps.citilink.selenium import CitilinkSelenium


class Citilink(CitilinkBeautifulSoup, CitilinkSelenium):
    img = 'https://3dnews.ru/assets/external/illustrations/2019/06/28/989931/sm.logo_new-01.800.jpg'
    url = 'https://www.citilink.ru'

    def __str__(self):
        return 'Citilink'
