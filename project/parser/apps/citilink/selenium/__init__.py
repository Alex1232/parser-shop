from time import sleep
from typing import NoReturn

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from parser.apps import InitialSelenium


class CitilinkSelenium(InitialSelenium):

    def get_url_for_parser(self) -> str:
        return super(CitilinkSelenium, self).get_url_for_parser()

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        return super(CitilinkSelenium, self).get_element_by_silenium(driver, type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        return super(CitilinkSelenium, self).get_html(driver)

    def create_options(self) -> webdriver.ChromeOptions():
        return super(CitilinkSelenium, self).create_options()

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        driver = super(CitilinkSelenium, self).create_web_driver(option)
        return driver

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str):
        return super(CitilinkSelenium, self).record_html_in_driver_by_url(driver, url)

    def initialization_search_by_html(self, driver) -> NoReturn:
        # TODO Сделать красиво
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH,
                 '/html/body/div[2]/div[2]/header/div[2]/div[2]/div[1]/div/div/form/div/div[1]/div/label/input')))\
            .send_keys(
            f'{self.product}')
        sleep(2)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH,
                 '/html/body/div[2]/div[2]/header/div[2]/div[2]/div[1]/div/div/form/div/div[1]/div/label/input')))\
            .send_keys(Keys.ENTER)
        super(CitilinkSelenium, self).initialization_search_by_html(driver)