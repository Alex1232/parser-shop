
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class CitilinkBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'product_data__gtm-js product_data__pageevents-js ProductCardVertical js--ProductCardInListing ProductCardVertical_normal ProductCardVertical_shadow-hover ProductCardVertical_separated'})

    def get_clean_link(self, product_card):
        try:
            return f"{self.url}{product_card.find('a', {'class': 'ProductCardVertical__name Link js--Link Link_type_default'}).attrs['href']}",
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return 'https://valbergsafe.ru/upload/iblock/035/0355c33841b48ee7864bee9f531f12f2.jpg',
        except AttributeError:
            raise

    def get_clean_price(self, product_card):
        try:
            return product_card.find('span', {
                'class': 'ProductCardVerticalPrice__price-current_current-price js--ProductCardVerticalPrice__price-current_current-price'
            }).text.strip()
        except AttributeError:
            raise

    def get_clean_brand_name(self, product_card):
        return ''

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('a', {'class': 'ProductCardVertical__name Link js--Link Link_type_default'}).text,
        except AttributeError:
            raise

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(CitilinkBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)