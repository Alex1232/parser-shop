
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class KazanexpressBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'col-mbs-12 col-mbm-6 col-xs-4 col-md-3'})

    def get_clean_link(self, product_card):
        try:
            return f"{self.url}{product_card.find('a', {'class': 'tap-noselect noselect'}).attrs['href']}",
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return product_card.find('img', {
                'class': 'main-card-icon-and-classname-collision-made-to-minimum'
            }).attrs['src'],
        except AttributeError:
            raise

    def get_clean_price(self, product_card):
        try:
            return product_card.find('span', {
                'class': 'currency product-card-price slightly medium'
            }).text.strip()[:-1].strip()
        except AttributeError:
            raise

    def get_clean_brand_name(self, product_card):
        return ''

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('span', {'data-test-id': 'text__product-name'}).text,
        except AttributeError:
            raise

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(KazanexpressBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)