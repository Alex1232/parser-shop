from parser.apps.kazan_express.beautiful_soup import KazanexpressBeautifulSoup
from parser.apps.kazan_express.selenium import KazanexpressSelenium


class Kazanexpress(KazanexpressBeautifulSoup, KazanexpressSelenium):
    img = 'https://favo24.ru/wp-content/uploads/2021/KazanExpress_%D0%BB%D0%BE%D0%B3%D0%BE.png'
    url = 'https://kazanexpress.ru'

    def __str__(self):
        return 'Kazanexpress'
