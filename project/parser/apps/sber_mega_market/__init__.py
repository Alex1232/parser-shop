
from parser.apps.sber_mega_market.beautiful_soup import SberMegaMarketBeautifulSoup
from parser.apps.sber_mega_market.selenium import SberMegaMarketSelenium


class SberMegaMarket(SberMegaMarketBeautifulSoup, SberMegaMarketSelenium):
    img = 'https://api.halvacard.ru/public-api/files/ec61e512-2d39-4e24-b1fb-47e4b41a51c4.JPG'
    url = 'https://sbermegamarket.ru/'

    def __str__(self):
        return 'SberMegaMarket'
