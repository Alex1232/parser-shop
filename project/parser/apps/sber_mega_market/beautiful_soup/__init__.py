
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class SberMegaMarketBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'catalog-item ddl_product'})

    def get_clean_link(self, product_card):
        try:
            return f"{self.url}{product_card.find('a', {'class': 'ddl_product_link'}).attrs['href']}",
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return product_card.find('img', {'class': 'lazy-img'}).attrs['src'],
        except AttributeError and KeyError:
            return ' '

    def get_clean_price(self, product_card):
        try:
            return product_card.find('div', {'class': 'item-price'}).text.strip()[:-1].strip()
        except AttributeError:
            return ' '

    def get_clean_brand_name(self, product_card):
        return product_card.find('div', {'class': 'x-product-card-description__brand-name'}).text[:-3]

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('div', {'class': 'item-title desktop-only'}).text,
        except AttributeError:
            return ' '

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(SberMegaMarketBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)