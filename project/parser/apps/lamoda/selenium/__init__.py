from typing import NoReturn

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from parser.apps import InitialSelenium


class LamodaSelenium(InitialSelenium):

    def get_url_for_parser(self) -> str:
        return super(LamodaSelenium, self).get_url_for_parser()

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        return super(LamodaSelenium, self).get_element_by_silenium(driver, type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        return super(LamodaSelenium, self).get_html(driver)

    def create_options(self) -> webdriver.ChromeOptions():
        return super(LamodaSelenium, self).create_options()

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        return super(LamodaSelenium, self).create_web_driver(option)

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str):
        return super(LamodaSelenium, self).record_html_in_driver_by_url(driver, url)

    def initialization_search_by_html(self, driver) -> NoReturn:
        # TODO Сделать красиво
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//*[@id="vue-root"]/header/div[3]/div/div/div/div/input'))).send_keys(
            f'{self.product}')
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//*[@id="vue-root"]/header/div[3]/div/div/div/div/input'))).send_keys(Keys.ENTER)
        super(LamodaSelenium, self).initialization_search_by_html(driver)