
from parser.apps.lamoda.beautiful_soup import LamodaBeautifulSoup
from parser.apps.lamoda.selenium import LamodaSelenium


class Lamoda(LamodaBeautifulSoup, LamodaSelenium):
    img = 'https://cdn5.vedomosti.ru/image/2019/2c/wc9qq/original-15wx.jpg'
    url = 'https://www.lamoda.ru/'

    def __str__(self):
        return 'Lamoda'
