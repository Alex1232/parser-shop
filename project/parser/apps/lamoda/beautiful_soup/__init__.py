
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class LamodaBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'x-product-card__card'})

    def get_clean_link(self, product_card):
        try:
            return f"{self.url}{product_card.find('a', {'class': 'wCjUeog4KtWw64IplV1e6 _3dch7Ytt3ivpea7TIsKVjb x-product-card__pic x-product-card__pic-catalog'}).attrs['href']}",
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return product_card.find('img', {'class': '_3suRyOINTHUpTPTxxwWUTF x-product-card__pic-img'}).attrs['src'],
        except AttributeError:
            return product_card.find('img', {'class': '_3suRyOINTHUpTPTxxwWUTF uT0zfDfc_ceBSXXed1BK7 x-product-card__pic-img'}).attrs['src']

    def get_clean_price(self, product_card):
        try:
            return product_card.find('span', {'class': 'x-product-card-description__price-single x-product-card-description__price-WEB8507_price_no_bold'}).text.strip()[:-1].strip()
        except AttributeError:
            return ' '

    def get_clean_brand_name(self, product_card):
        return product_card.find('div', {'class': 'x-product-card-description__brand-name'}).text[:-3]

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('div', {'class': 'x-product-card-description__product-name'}).text,
        except AttributeError:
            return ' '
    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(LamodaBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)