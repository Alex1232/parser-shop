from time import sleep
from typing import NoReturn

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from parser.apps import InitialSelenium


class AliExpressSelenium(InitialSelenium):

    def get_url_for_parser(self) -> str:
        return super(AliExpressSelenium, self).get_url_for_parser()

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        return super(AliExpressSelenium, self).get_element_by_silenium(driver, type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        return super(AliExpressSelenium, self).get_html(driver)

    def create_options(self) -> webdriver.ChromeOptions():
        return super(AliExpressSelenium, self).create_options()

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        return super(AliExpressSelenium, self).create_web_driver(option)

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str):
        return super(AliExpressSelenium, self).record_html_in_driver_by_url(driver, url)

    def initialization_search_by_html(self, driver) -> NoReturn:
        # TODO Сделать красиво
        sleep(4)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '/html/body/div[1]/div/div[2]/div/div/div[2]/div/form/div/div/input'))).send_keys(
            f'{self.product}')
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '/html/body/div[1]/div/div[2]/div/div/div[2]/div/form/div/div/input'))).send_keys(Keys.ENTER)
        super(AliExpressSelenium, self).initialization_search_by_html(driver)