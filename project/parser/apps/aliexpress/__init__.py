
from parser.apps.aliexpress.beautiful_soup import AliExpressBeautifulSoup
from parser.apps.aliexpress.selenium import AliExpressSelenium


class AliExpress(AliExpressBeautifulSoup, AliExpressSelenium):
    img = 'https://marketer.ua/wp-content/uploads/2017/02/14776700062170.png'
    url = 'https://aliexpress.ru/'

    def __str__(self):
        return 'AliExpress'
