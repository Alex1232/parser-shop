
from typing import List, Dict

from parser.apps import InitialBeautifulSoup


class AliExpressBeautifulSoup(InitialBeautifulSoup):
    def get_card_wrapper(self, soup):
        return soup.find_all('div', {'class': 'SearchProductFeed_ProductSnippet__content__7lwrv'})

    def get_clean_link(self, product_card):
        try:
            return f"{self.url}{product_card.find('a', {'class': 'SearchProductFeed_ProductSnippet__galleryBlock__7lwrv'}).attrs['href']}",
        except AttributeError:
            raise

    def get_clean_image(self, product_card):
        try:
            return product_card.find('img', {'class': 'SearchProductFeed_Gallery__image__1ln22'}).attrs['src'],
        except AttributeError and KeyError:
            return ' '

    def get_clean_price(self, product_card):
        try:
            return product_card.find('div', {'class': 'snow-price_SnowPrice__mainM__2y0jkd'}).text.strip()[:-1].strip()
        except AttributeError:
            return ' '

    def get_clean_brand_name(self, product_card):
        try:
            return product_card.find('div', {'class': 'x-product-card-description__brand-name'}).text[:-3]
        except AttributeError:
            return ''

    def get_clean_goods_name(self, product_card):
        try:
            return product_card.find('div', {'class': 'SearchProductFeed_ProductSnippet__name__7lwrv'}).text,
        except AttributeError:
            return ' '

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        return super(AliExpressBeautifulSoup, self).get_list_parsed_data(product_card_wrapper)