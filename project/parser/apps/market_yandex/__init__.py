from parser.apps.market_yandex.beautiful_soup import MarketYandexBeautifulSoup
from parser.apps.market_yandex.selenium import MarketYandexSelenium


class MarketYandex(MarketYandexBeautifulSoup, MarketYandexSelenium):
    img = 'https://www.ecosum.ru/wp-content/uploads/yandex-market.png'
    url = 'https://market.yandex.ru'


    def __str__(self):
        return 'MarketYandex'
