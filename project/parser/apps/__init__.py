from time import sleep
from typing import (List, Dict, NoReturn)

from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class BaseInitial:
    """Основной класс для всех моделей магазинов"""
    def __init__(self, product, checker):
        self.product = product
        self.checker = checker


class InitialBeautifulSoup(BaseInitial):

    def get_card_wrapper(self, soup):
        """
        Общий метод класса - отдает обертку для всех продуктов
        """
        pass

    def get_list_parsed_data(self, product_card_wrapper) -> List[Dict[str, str]]:
        """
        Общий метод класса - отдает список словарей, который
        содержит в себе информацию о товаре в формате {ключ-значение}
        """
        list_parser_data = []
        i = 0
        for product_card in product_card_wrapper:
            price: str = self.get_clean_price(product_card)
            brand_name: str = self.get_clean_brand_name(product_card)
            # Проверяю наличие цены, принадлежность с нужной категории и срезаю все товары до 20 штук
            if price and i < 5:
                i += 1
                list_parser_data.append(
                    {
                        'id': i,
                        'link': self.get_clean_link(product_card)[0],
                        'image': self.get_clean_image(product_card)[0],
                        'price': price,
                        'brand_name': brand_name,
                        'goods_name': self.get_clean_goods_name(product_card)[0][:110],
                    }
                )
        return list_parser_data


class InitialSelenium(BaseInitial):

    def get_url_for_parser(self) -> str:
        """Метод получает url адрес из settings"""
        return self.url

    def get_element_by_silenium(self, driver, type_element: By, name_element: str):
        """Метод отдает элемент браузера"""
        return driver.find_element(type_element, name_element)

    def get_html(self, driver) -> WebDriver:
        """Метод возвращает html страницу"""
        return driver.page_source

    def create_options(self) -> webdriver.ChromeOptions():
        """Метод создает option для Chrome (чтобы не инициировать открытие браузера)"""
        option = webdriver.ChromeOptions()
        option.add_argument("start-maximized")
        option.add_argument("disable-infobars")
        option.add_argument("--disable-extensions")
        option.add_argument("--disable-blink-features=AutomationControlled")
        # option.add_argument('--headless')
        return option

    def create_web_driver(self, option: webdriver.ChromeOptions()) -> WebDriver:
        """Метод создает объект браузера"""
        # Настройки для неполной загрузки страницы
        caps = DesiredCapabilities().CHROME
        caps["pageLoadStrategy"] = "eager"
        return webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=option, desired_capabilities=caps)

    def record_html_in_driver_by_url(self, driver: WebDriver, url: str) -> NoReturn:
        """Метод записывает html страницу в объект WebDriver"""
        driver.get(url)
        # Жду чтобы все прогрузилось
        sleep(1)

    def initialization_search_by_html(self, driver) -> NoReturn:
        """Метод инициализирует поиск по сайту"""
        sleep(3)