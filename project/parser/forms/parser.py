from django import forms


class ParserForm(forms.Form):
    product = forms.CharField(label='Искать товары')
    product.widget.attrs.update({
        'class': 'form-control rounded',
        'type': 'search',
        'placeholder': 'Искать товары',
        'aria-label': 'Search',
        'aria-describedby': 'search-addon',
        'style': 'height: 45px;'
    })