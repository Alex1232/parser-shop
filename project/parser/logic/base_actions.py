from typing import (List, Dict)

from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver


def get_html_with__silenium(shop_obj) -> WebDriver:
    url: str = shop_obj.get_url_for_parser()
    option: Options = shop_obj.create_options()
    driver: WebDriver = shop_obj.create_web_driver(option)

    shop_obj.record_html_in_driver_by_url(driver, url)
    shop_obj.initialization_search_by_html(driver)

    html = shop_obj.get_html(driver)

    driver.quit()
    return html


def processing_html_with__beautiful_soup(html, shop_obj) -> List[Dict[str, str]]:
    soup = BeautifulSoup(html, 'lxml')
    product_card_wrapper: list = shop_obj.get_card_wrapper(soup)
    return shop_obj.get_list_parsed_data(product_card_wrapper)


def action_with_data(shop_obj) -> List[Dict[str, str]]:
    html: WebDriver = get_html_with__silenium(shop_obj)
    return processing_html_with__beautiful_soup(html, shop_obj)