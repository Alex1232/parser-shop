from django.template.response import SimpleTemplateResponse


def get_obj_template_response(response_parser_data):
    """Метод отдает объект Template"""
    return SimpleTemplateResponse(
        template='base/include/body.html',
        context=response_parser_data
    )


def get_resolve_template(obj):
    """Метод отдает отрендеренный html"""
    template = obj.resolve_template(obj.template_name)
    context = obj.resolve_context(obj.context_data)
    return template.render(context, obj._request)
