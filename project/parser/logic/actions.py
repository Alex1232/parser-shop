from django.http import JsonResponse

from parser_shop import settings


def check_request_data(checker) -> JsonResponse:
    """Метод проверяет - пришли ли все нужные данные с фронта"""
    return \
        JsonResponse({'error': 'Выбирите магазин'}) if not checker else \
        JsonResponse({'error': 'Введите название продукта'})


def get_shop_obj(product, checker):
    """Метод отдает объект магазина по checker"""
    return settings.SHOPS.get(checker)(product, checker)