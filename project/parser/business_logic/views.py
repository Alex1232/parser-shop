from django.http import JsonResponse
from django.views.generic.edit import FormView

from parser.forms.parser import ParserForm
from parser.logic.actions import (check_request_data, get_shop_obj)
from parser.logic.base_actions import action_with_data
from parser.logic.getters import (get_obj_template_response, get_resolve_template)
from parser_shop import settings


class ParserFormView(FormView):
    template_name = 'parser/forms/parser.html'
    form_class = ParserForm
    success_url = '/'

    def get_context_data(self):
        super_form = super(ParserFormView, self).get_context_data()
        super_form['form'].data['checkbox_shops'] = settings.SHOPS
        super_form['form'].data['image_link'] = f'{settings.MEDIA_URL}/PS.png'
        super_form['form'].data['image_PS'] = f'{settings.MEDIA_URL}/ParserShop.png'
        super_form['form'].fields['product'].widget.input_type = 'search'
        return super_form


def collect_parser_data(request):
    # Собираю данные с интернет-магазинов
    product = request.GET['product']
    checker = request.GET['checker']

    categories = [
        'Электроника',
        'Аксессуары',
        'Дом и сад',
        'Хобби и творчество',
        'Мебель',
        'Автотовары',
        'Канцелярские товары',
        'Детские товары',
        'Строительство и ремонт',
        'Спорт и отдых',
        'Туризм рыбалка охота',
        'Книги',
        'Игры и консоли',
        'Бытовая техника',
        'Антиквариат и коллекционирование',
        'Одежда',
        'Красота и здоровье',
        'Аптека',
        'Бытовая химия',
        'Товары для животных',
    ]

    filters = [
        'Цены',
        'Бренд',
        'Продавец',
        'Высокий рейтинг',
        'Товары со скидкой',
    ]

    check_request_data(checker)

    shop_obj = get_shop_obj(product, checker)
    response_parser_data = {
        'result_parser': action_with_data(shop_obj),
        'img': shop_obj.img,
        'product_search': product,
        'categories': categories,
        'filters': filters,
    }

    if not response_parser_data['result_parser']:
        return JsonResponse(
            {
                'error': 'К сожалению ничего не найденно &#128532; <br/>'
                         'Попробуйте - сменить магазин, ввести другой товар или перефразировать запрос. <br/>'
                         'Если вы все перепробовали но так же ничего не можете найти '
                         '- обратитесь к нам - supporparsershop@mail.ru'
            }
        )

    obj = get_obj_template_response(response_parser_data)
    html = get_resolve_template(obj)

    return JsonResponse(html, safe=False)